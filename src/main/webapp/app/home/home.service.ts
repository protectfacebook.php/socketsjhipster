import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class HomeService {
  socket: any;
  server = 'http://localhost:4000';

  constructor() {
    this.socket = io(this.server);
  }

  listenConnect(eventName: string): Observable<any> {
    return new Observable<any>(Subscriber => {
      this.socket.on(eventName, (data: any) => {
        Subscriber.next(data);
      });
    });
  }

  listenDate(eventName: string): Observable<any> {
    return new Observable<any>(Subscriber => {
      this.socket.on(eventName, (data: any) => {
        Subscriber.next(data);
      });
    });
  }
}
