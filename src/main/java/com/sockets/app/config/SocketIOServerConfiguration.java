package com.sockets.app.config;

import com.corundumstudio.socketio.SocketConfig;
import com.corundumstudio.socketio.SocketIOServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SocketIOServerConfiguration {

    @Bean
    public SocketIOServer socketIOServer(){
        SocketConfig config =  new SocketConfig();
        config.setTcpNoDelay(true);
        config.setSoLinger(0);
        com.corundumstudio.socketio.Configuration configuration =
            new com.corundumstudio.socketio.Configuration();
        configuration.setSocketConfig(config);
        configuration.setHostname("127.0.0.1");
        configuration.setPort(4000);
        configuration.setBossThreads(1);
        configuration.setWorkerThreads(1000);
        configuration.setAllowCustomRequests(true);
        configuration.setUpgradeTimeout(1000000);
        configuration.setPingTimeout(6000000);
        configuration.setPingInterval(25000);
        return new SocketIOServer(configuration);
    }
}
