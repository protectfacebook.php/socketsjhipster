/**
 * View Models used by Spring MVC REST controllers.
 */
package com.sockets.app.web.rest.vm;
