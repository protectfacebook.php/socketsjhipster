package com.sockets.app.web.rest.WebSocket;

public interface ISocketIOService {
    public void start();
    public void stop();
}
