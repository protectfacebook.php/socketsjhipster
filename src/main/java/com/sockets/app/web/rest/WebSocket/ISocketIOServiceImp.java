package com.sockets.app.web.rest.WebSocket;

import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import org.mapstruct.ap.shaded.freemarker.template.utility.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service(value = "socketIOService")
public class ISocketIOServiceImp implements ISocketIOService{

    public static Map<String, SocketIOClient> clientMap = new HashMap<>();

    @Autowired
    private SocketIOServer socketIOServer;

    @PostConstruct
    public void  autoStart(){
        start();
    }

    @PreDestroy
    public  void shutDownSocket(){
        stop();
    }

    @Override
    public void start() {
        socketIOServer.addConnectListener(client -> {
            client.sendEvent("connected", "you are conected");
            enviarData();
        });
        socketIOServer.addDisconnectListener(client -> {
            String userId = getParamsByClient(client);
            if (userId != null) {
                clientMap.remove(userId);
                client.disconnect();
            }
        });
        socketIOServer.start();
    }

    @Override
    public void stop() {
        if (socketIOServer != null) {
            socketIOServer.stop();
            socketIOServer = null;
        }
    }
    private String getParamsByClient(SocketIOClient client) {
        // Get the client url parameter (where userId is the unique identity)
        Map<String, List<String>> params = client.getHandshakeData().getUrlParams();
        List<String> userIdList = params.get("userId");
        if (!CollectionUtils.isEmpty(userIdList)) {
            return userIdList.get(0);
        }
        return null;
    }
    private void  enviarData(){
        new Thread(() -> {
            int i = 0;
            while (true) {
                try {
                    // Send broadcast message every 3 seconds
                    Thread.sleep(3000);
                    socketIOServer.getBroadcastOperations().sendEvent("myBroadcast", "Broadcast message " + LocalDate.now());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


}
